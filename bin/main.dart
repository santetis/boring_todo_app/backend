import 'package:backend/backend.dart';
import 'package:grpc/grpc.dart';
import 'package:database_interface/database_interface.dart';
import 'package:mongo_database/mongo_database.dart';

Future<void> main(List<String> arguments) async {
  final dbManager = NoSqlDatabaseManager(() => MongoDatabase(mongoUrl));
  final pool = NoSqlDatabasePool.fromManager(dbManager);
  final services = [
    AccountService(pool),
  ];
  final server = Server(services);
  await server.serve(port: 8081);
  print('Server listening on port ${server.port}');
}

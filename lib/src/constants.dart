const mongoUrl = const String.fromEnvironment(
  'MONGO_URL',
  defaultValue: 'mongodb://localhost:27017/alfred',
);

const secretKey = String.fromEnvironment(
  'SECRET_KEY',
  defaultValue: 'abcdefghijklmnopqrstuvwxyz',
);


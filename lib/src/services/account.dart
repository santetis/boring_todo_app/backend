import 'package:backend/src/constants.dart';
import 'package:database_interface/database_interface.dart';
import 'package:generated_protos/generated_protos.dart';
import 'package:grpc/grpc.dart';
import 'package:crypto/crypto.dart';
import 'package:uuid/uuid.dart';

class AccountService extends AccountServiceBase {
  NoSqlDatabasePool pool;

  AccountService(this.pool);

  @override
  Future<SignInResponse> signIn(ServiceCall call, SignInRequest request) async {
    final connection = await pool.pool.get();
    final db = connection.connection;
    final account = db.collection('accounts');
    final hasher = Hmac(sha256, secretKey.codeUnits);
    final data = await account.findOne({
      'email': request.email,
      'password': hasher.convert(request.password.codeUnits)
    });
    if (data == null) {
      await connection.release();
      throw GrpcError.notFound();
    }
    final token = Uuid().v5('santetis.fr', 'boringtodoapp', options: {
      'now': DateTime.now(),
    });
    data['token'] = token;
    await account.update({'_id': data['_id']}, data);
    await connection.release();
    return SignInResponse()..token = token;
  }

  @override
  Future<Empty> signUp(ServiceCall call, SignUpRequest request) async {
    final connection = await pool.pool.get();
    final db = connection.connection;
    final account = db.collection('accounts');
    final data = await account.findOne({'email': request.email});
    if (data != null) {
      await connection.release();
      throw GrpcError.alreadyExists();
    }
    final hasher = Hmac(sha256, secretKey.codeUnits);
    await account.insert({
      'email': request.email,
      'password': hasher.convert(request.password.codeUnits),
    });
    await connection.release();
    return Empty();
  }
}

import 'package:generated_protos/generated_protos.dart';
import 'package:database_interface/database_interface.dart';

Future<Account> getUserWithAuthorization(
    NoSqlDatabaseInterface db, String authorizzation) async {
  final collection = db.collection('accounts');
  final user = await collection.findOne({'token': authorizzation});
  if (user == null) {
    return null;
  }
  return Account()
    ..id = (user['_id'] as DatabaseId).id
    ..email = user['email']
    ..password = user['password']
    ..token = user['token'];
}
